import tempfile

from astropy.table import Table, Row, unique
from astropy.io import ascii
import numpy as np
from cycler import cycler
from tqdm import tqdm


class AlchemicInput:
    def __init__(
            self,
            gas_temperature,
            gas_density,
            uv_field,
            stellar_av=0,
            interstellar_av=0,
            zeta_cr=1e-18,
            zeta_x=0,
            dust_temperature=None,
            dust_to_gas_mass_ratio=0.01,
            dust_size_cm=1e-5,
            is_h2_self_shielding=1,
            is_co_self_shielding=1,
            st_h2_self_shielding=1,
            st_co_self_shielding=1,
            header="Radius,Height,Gas density,Gas temperature,Dust temperature,a_d,Dust to gas,G0,"
                   "Stellar Av,Interstellar Av,ZetaCR,ZetaX,"
                   "IS H2 self-shielding,IS CO self-shielding,St H2 self-shielding,St CO self-shielding,Scale height"
    ):
        self.radius = cycler("Radius", [1])
        self.height = cycler("Height", [1])
        self.gas_temperature = cycler("Gas temperature", np.array(gas_temperature, ndmin=1))
        self.gas_density = cycler("Gas density", np.array(gas_density, ndmin=1))
        self.uv_field = cycler("G0", np.array(uv_field, ndmin=1))
        self.stellar_av = cycler("Stellar Av", np.array(stellar_av, ndmin=1))
        self.interstellar_av = cycler("Interstellar Av", np.array(interstellar_av, ndmin=1))
        self.zeta_cr = cycler("ZetaCR", np.array(zeta_cr, ndmin=1))
        self.zeta_x = cycler("ZetaX", np.array(zeta_x, ndmin=1))

        self.cycler = self.radius * self.height * self.gas_density

        if dust_temperature is not None:
            self.dust_temperature = cycler("Dust temperature", np.array(dust_temperature, ndmin=1))
            self.cycler *= self.gas_temperature * self.dust_temperature
        else:
            self.dust_temperature = cycler("Dust temperature", np.array(gas_temperature, ndmin=1))
            self.cycler *= self.gas_temperature + self.dust_temperature

        self.dust_to_gas_mass_ratio = cycler("Dust to gas", np.array(dust_to_gas_mass_ratio, ndmin=1))
        self.dust_size_cm = cycler("a_d", np.array(dust_size_cm, ndmin=1))

        self.cycler *= (
                self.dust_size_cm * self.dust_to_gas_mass_ratio
        )

        self.is_h2_self_shielding = cycler("IS H2 self-shielding", np.array(is_h2_self_shielding, ndmin=1))
        self.is_co_self_shielding = cycler("IS CO self-shielding", np.array(is_co_self_shielding, ndmin=1))

        self.st_h2_self_shielding = cycler("St H2 self-shielding", np.array(st_h2_self_shielding, ndmin=1))
        self.st_co_self_shielding = cycler("St CO self-shielding", np.array(st_co_self_shielding, ndmin=1))

        self.scale_hight = cycler("Scale height", [0])

        self.cycler *= (
                self.uv_field * self.stellar_av * self.interstellar_av *
                self.zeta_cr * self.zeta_x *
                self.is_h2_self_shielding * self.is_co_self_shielding * self.st_h2_self_shielding * self.st_co_self_shielding *
                self.scale_hight
        )

        if isinstance(header, str):
            keys = header.split(',')
        else:
            keys = next(self.cycler).keys()

        print(f"Cycler of length {len(self.cycler)} created! ", self.cycler)
        self.table = Table(names=keys, data=np.empty((len(self.cycler), len(keys)), dtype=float))
        for i, element in tqdm(enumerate(self.cycler), desc="Creating table",
                               total=len(self.cycler)):
            row = [element[key] for key in keys]
            self.table[i] = row
        self.table = unique(self.table)
        self.table['Height'] = np.arange(len(self.table))
        self.table.meta = {"comment":f"1\n{len(self.table)}"}


if __name__ == '__main__':
    ai = AlchemicInput(
        gas_temperature=np.geomspace(10, 300, 10),
        gas_density=np.geomspace(1e3, 1e7, 21) / 6.02e23, # gcm-3 to Hcm-3
        interstellar_av=np.linspace(0, 0.04, 5),
        stellar_av=np.linspace(0, 1, 5),
        dust_to_gas_mass_ratio=np.array([1e-10, 1e-2]),
        uv_field=np.geomspace(1e2, 1e6, 5)
    )
    # ai.table.pprint()
    tempfile = tempfile.mkstemp()[1]
    finfile = "1environ_0d.inp"
    ascii.write(
        ai.table, tempfile,
        format="fixed_width", overwrite=True, delimiter=" ",
        formats={colname: "%12.7e" for colname in ai.table.colnames},
        comment=ai.table.meta['comment']
    )
    with open(tempfile, 'r') as tmpfl:
        with open(finfile, 'w') as finfl:
            print(tmpfl.readline(), file=finfl)
            print(len(ai.radius), file=finfl)
            print(len(ai.table) // len(ai.radius), file=finfl)
            for line in tmpfl.readlines():
                print(line.strip(), file=finfl)
