import contextlib
import io
import sys
from dataclasses import dataclass

import numpy as np
import astropy.io.ascii
from astropy import units as u
from tqdm import tqdm

from pyndes.radexpy.radex import get_output_table


@dataclass
class Transition:
    name: str
    file: str
    frequency_range: tuple = (160, 260)


@contextlib.contextmanager
def nostdout():
    save_stdout = sys.stdout
    sys.stdout = io.BytesIO()
    yield
    sys.stdout = save_stdout


if __name__ == '__main__':
    file_to_process = sys.argv[1]
    output_file = sys.argv[2]
    radex = sys.argv[3]

    print(f"Processing file {file_to_process}")
    tbl = astropy.io.ascii.read(file_to_process, format='fixed_width', delimiter='|')
    print(f"{file_to_process} read to astropy.table")

    tbl['Self-shielding'] = np.arange(len(tbl)) % 2

    # transitions = {
    #     'CO 2-1': 'co.dat',
    #     '13CO 2-1': '13co.dat',
    #     'C18O 2-1': 'c18o.dat',
    #     'HCO+ 2-1': 'hco+.dat',
    # }

    transitions = [
        Transition('CO J=2-1', 'co.dat'),
        Transition('13CO J=2-1', '13co.dat'),
        Transition('C18O J=2-1', 'c18o.dat'),
        Transition('HCO+ J=2-1', 'hco+@xpol.dat'),
        Transition('CN N=2-1', 'cn.dat', (220, 230)),
        Transition('HCN J=2-1', 'hcn.dat', (170, 180)),
        # Transition('C2H N=2-1', 'c2h_h2_e.dat', (170, 180)),
        # Transition('CS J=5-4', 'cs@lique.dat', (240, 250)),
    ]

    tbl['13CO'] = tbl['CO'] / 70
    tbl['C18O'] = tbl['CO'] / 550

    for transition in transitions:
        tbl[transition.name] = np.nan

    for entry in tqdm(tbl):
        for transition in transitions:
            try:
                radex_table = get_output_table(
                    transition.file,
                    minimum_frequency=transition.frequency_range[0] * u.GHz,
                    maximum_frequency=transition.frequency_range[1] * u.GHz,
                    kinetic_temperature=entry['Tg'] * u.K,
                    molecular_column_density=(
                            entry[transition.name.split()[0]] * entry['n(H+2H2)'] * u.cm ** -3
                            * 100. * u.au
                    ),
                    collision_partners=(
                        ("H2", entry['n(H+2H2)'] * entry['H2'] * u.cm ** -3),
                    ),
                    executable=radex,
                    temporary_file_kwargs={'dir': '.'}
                )
                entry[transition.name] = np.sum(radex_table['FLUX'])
            except ValueError:
                pass
            # print(f"\nEntry failed! \n{entry['Tg', 'n(H+2H2)', 'H2', 'CO']}")
        # break
    tbl.write(output_file, overwrite=True, format='ascii.fixed_width', delimiter='|')
