"""Self-shielding is multiplier of UV on the photodissociation reaction of H2 and/or CO"""

import sys

import numpy as np
import astropy.io.ascii

if __name__ == '__main__':
    file_to_process = sys.argv[1]
    output_file = sys.argv[2]
    print(f"Processing file {file_to_process}")
    tbl = astropy.io.ascii.read(file_to_process, format='fixed_width', delimiter='|')
    print(f"{file_to_process} read to astropy.table")
    tbl['Self-shielding'] = np.arange(len(tbl)) % 2
    tbl.write(output_file, overwrite=True, format='ascii.fixed_width', delimiter='|')
