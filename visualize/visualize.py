"""Script to visualize the output of alchemic grid"""

import sys
from dataclasses import dataclass
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize, LogNorm
from matplotlib.cm import get_cmap
import astropy.io.ascii
import pandas as pd
from tqdm import tqdm


def filter_by_dust2gas(df, more, d2g=1e-3, name='Mdust/Mgas'):
    if more:
        return df[df[name] >= d2g]
    else:
        return df[df[name] < d2g]


def filter_by_av(df, more, av=0.5, name='Av(stellar)'):
    if more:
        return df[df[name] >= av]
    else:
        return df[df[name] < av]


def filter_by_ss(df, less, limit=0.5, name='Self-shielding'):
    """Self-shielding is multiplier of UV on the photodissociation reaction of H2 and/or CO
    0 means shielded, 1 means not-shielded!"""
    if less:
        return df[df[name] <= limit]
    else:
        return df[df[name] > limit]


@dataclass
class Vis:
    name: str
    column: str
    normalizer: Normalize = Normalize()
    colormap: str = "tab10"
    observed_limit: float = None
    all_possible: bool = False

    def __post_init__(self):
        if self.observed_limit:
            self.normalizer.vmax = self.observed_limit

if __name__ == '__main__':
    file_to_process = sys.argv[1]
    output_file = sys.argv[2]

    subplot_kw = {
        "xlim": (7, 400),
        "ylim": (1e-21, 1e-16)
    }
    print(f"Processing file {file_to_process}")
    tbl = astropy.io.ascii.read(file_to_process, format='fixed_width', delimiter='|')
    print(f"{file_to_process} read to astropy.table")
    df = tbl.to_pandas()

    norm = Normalize(-8, -3)
    cols = ['No dust', 'With dust']
    rows = ['No self-shielding', 'With self-shielding']

    normalizer_co = LogNorm(1e-10, 1e-2)
    df['CO_normalized'] = normalizer_co(df['CO'].to_numpy()) * 10
    df['log_rho_g'] = np.log10(df['rho_g'])

    df['HCO+/CO'] = df['HCO+'] / df['CO']
    df['log(HCO+/CO)'] = np.log10(df['HCO+/CO'])
    df['HCO+ J=2-1/CO J=2-1'] = df['HCO+ J=2-1'] / df['CO J=2-1']
    df['log(HCO+ J=2-1/CO J=2-1)'] = np.log10(df['HCO+ J=2-1/CO J=2-1'])
    df['HCO+ J=2-1/C18O J=2-1'] = df['HCO+ J=2-1'] / df['C18O J=2-1']
    df['log(HCO+ J=2-1/C18O J=2-1)'] = np.log10(df['HCO+ J=2-1/C18O J=2-1'])
    df['HCO+ J=2-1/13CO J=2-1'] = df['HCO+ J=2-1'] / df['13CO J=2-1']
    df['log(HCO+ J=2-1/13CO J=2-1)'] = np.log10(df['HCO+ J=2-1/13CO J=2-1'])

    df['CN/CO'] = df['CN'] / df['CO']
    df['log(CN/CO)'] = np.log10(df['CN/CO'])
    df['CN N=2-1/CO J=2-1'] = df['CN N=2-1'] / df['CO J=2-1']
    df['log(CN N=2-1/CO J=2-1)'] = np.log10(df['CN N=2-1/CO J=2-1'])
    df['CN N=2-1/C18O J=2-1'] = df['CN N=2-1'] / df['C18O J=2-1']
    df['log(CN N=2-1/C18O J=2-1)'] = np.log10(df['CN N=2-1/C18O J=2-1'])
    df['CN N=2-1/13CO J=2-1'] = df['CN N=2-1'] / df['13CO J=2-1']
    df['log(CN N=2-1/13CO J=2-1)'] = np.log10(df['CN N=2-1/13CO J=2-1'])

    df['HCN/CO'] = df['HCN'] / df['CO']
    df['log(HCN/CO)'] = np.log10(df['HCN/CO'])
    df['HCN J=2-1/CO J=2-1'] = df['HCN J=2-1'] / df['CO J=2-1']
    df['log(HCN J=2-1/CO J=2-1)'] = np.log10(df['HCN J=2-1/CO J=2-1'])
    df['HCN J=2-1/C18O J=2-1'] = df['HCN J=2-1'] / df['C18O J=2-1']
    df['log(HCN J=2-1/C18O J=2-1)'] = np.log10(df['HCN J=2-1/C18O J=2-1'])
    df['HCN J=2-1/13CO J=2-1'] = df['HCN J=2-1'] / df['13CO J=2-1']
    df['log(HCN J=2-1/13CO J=2-1)'] = np.log10(df['HCN J=2-1/13CO J=2-1'])

    # df['xcoor'] = df['G0(stellar)'] * (0.6 + 0.8 * df['Av(stellar)'])
    # df['ycoor'] = df['log_rho_g']  + df['Tg'] / 50

    df['G0_normalized'] = LogNorm(1e2, 1e6)(df['G0(stellar)'].to_numpy())

    df['Gas density, gcm-3'] = df['rho_g'] * (0.8 + 0.4 * df['G0_normalized'])
    df['Temperature'] = df['Tg'] * (0.9 + 0.2 * df['G0_normalized'])

    colormap = get_cmap("viridis", 10)
    # colormap.set_over('red')

    plots = [
        Vis('abundance', 'log(HCO+/CO)', Normalize(-7, -2), colormap),
        Vis('co', 'log(HCO+ J=2-1/CO J=2-1)', Normalize(-7, -2), colormap),
        Vis('13co', 'log(HCO+ J=2-1/13CO J=2-1)', Normalize(-5, 0), colormap, observed_limit=np.log10(0.04)),
        Vis('c18o', 'log(HCO+ J=2-1/C18O J=2-1)', Normalize(-4, 1), colormap, observed_limit=np.log10(0.2)),

        Vis('abundance', 'log(CN/CO)', Normalize(-7, -2), colormap),
        Vis('co', 'log(CN N=2-1/CO J=2-1)', Normalize(-7, -2), colormap),
        Vis('13co', 'log(CN N=2-1/13CO J=2-1)', Normalize(-5, 0), colormap, observed_limit=np.log10(0.025)),
        Vis('c18o', 'log(CN N=2-1/C18O J=2-1)', Normalize(-4, 1), colormap, observed_limit=np.log10(0.1)),

        Vis('abundance', 'log(HCN/CO)', Normalize(-9, -4), colormap),
        Vis('co', 'log(HCN J=2-1/CO J=2-1)', Normalize(-7, -2), colormap),
        Vis('13co', 'log(HCN J=2-1/13CO J=2-1)', Normalize(-5, 0), colormap, observed_limit=np.log10(0.03)),
        Vis('c18o', 'log(HCN J=2-1/C18O J=2-1)', Normalize(-4, 1), colormap, observed_limit=np.log10(0.2)),

        Vis('abundance', 'log(HCO+/CO)', Normalize(-7, -2), colormap, all_possible=True),
    ]
    temperatures = sorted(set(df['Tg']))
    all_possible = set(df['Grid point'])
    with PdfPages(output_file) as pdf:
        for plot in tqdm(plots):
            possible_at_this_configuration = None
            fig, ax = plt.subplots(
                2, 1, sharex=True, sharey=True,
                subplot_kw=subplot_kw,
                squeeze=False,
                gridspec_kw={'hspace': 0.1, 'wspace': 0.1}
            )
            fig.suptitle(plot.column)
            for axis, col in zip(ax[0], cols):
                axis.set_title(col)
            for d2g in [0]:
                for ss in 0, 1:
                    df_touse = df.pipe(
                        filter_by_dust2gas, d2g  # >1e-3 if d2g
                    ).pipe(
                        filter_by_ss, ss  # with self-shielding if ss
                    ).pipe(
                        filter_by_av, 0, 0.01  # with av<0.01
                    )
                    if plot.all_possible:
                        df_touse = df_touse[df_touse['Grid point'].isin(all_possible)]
                    df_touse.plot.scatter(
                        x='Temperature', y='Gas density, gcm-3', c=plot.column, loglog=True,
                        colorbar=False,
                        colormap=plot.colormap,
                        norm=plot.normalizer,
                        ax=ax[ss, d2g],
                        s=df_touse['CO_normalized'],
                        marker='s'
                    )
                    if plot.observed_limit:
                        df_not_fits_observations = df_touse[df_touse[plot.column] > plot.observed_limit]
                        df_fits_observations = df_touse[
                            (df_touse[plot.column] <= plot.observed_limit) | (df_touse[plot.column].isna())
                            ]
                        below_limit = df_fits_observations['Grid point']
                        print(below_limit)
                        if possible_at_this_configuration is None:
                            possible_at_this_configuration = set(below_limit)
                        else:
                            possible_at_this_configuration |= set(below_limit)
                        print(possible_at_this_configuration)
                        df_not_fits_observations.plot.scatter(
                            x='Temperature', y='Gas density, gcm-3',
                            color='red',
                            ax=ax[ss, d2g],
                            s=10,
                            # alpha=0.5,
                            marker='x'
                        )
            for axis, row in zip(ax[:, 0], rows):
                axis.set_ylabel(row + "\n" + axis.get_ylabel())
            cbar = fig.colorbar(ax[0, 0].collections[0], ax=ax, label=plot.column)
            if plot.observed_limit:
                cbar.ax.axhline(plot.observed_limit, color='red')
                all_possible &= possible_at_this_configuration
                print(all_possible)
                # cbar.add_lines([plot.observed_limit])
            pdf.savefig(fig)
